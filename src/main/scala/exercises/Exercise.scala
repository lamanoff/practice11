package exercises

import matryoshka._
import matryoshka.data.Fix
import matryoshka.implicits._
import slamdata.Predef._

import scalaz.Functor

object Exercise extends App {

  sealed trait FileSystem[A]
  final case class Folder[A](size: Int, left: A, right: A) extends FileSystem[A]
  final case class File[A](size: Int)                      extends FileSystem[A]
  final case class FolderEmpty[A]()                        extends FileSystem[A]

  implicit val fileF: Functor[FileSystem] = new Functor[FileSystem] {
    def map[A, B](fa: FileSystem[A])(f: A => B): FileSystem[B] = fa match {
      case Folder(label, l, r) => Folder(label, f(l), f(r))
      case File(label)       => File[B](label)
      case FolderEmpty()       => FolderEmpty[B]()
    }
  }

  val fileSystem: Fix[FileSystem] = Fix(Folder(
    42,
    Fix(Folder(
      28,
      Fix(File(12)),
      Fix(Folder(
        32,
        Fix(File(29)),
        Fix(File(35))
      ))
    )),
    Fix(File(83))
  ))

  val coAlgebra: Coalgebra[FileSystem, List[Int]] = {
    case Nil          => FolderEmpty()
    case head :: Nil  => File(head)
    case head :: tail =>
      val (smaller, greater) = tail.partition(_ < head)
      Folder(head, smaller, greater)
  }

  val algebra: Algebra[FileSystem, List[Int]] = {
    case Folder(i, l, r) => l ++ List(i) ++ r
    case File(i)       => List(i)
    case FolderEmpty()   => Nil
  }


  //тест coAlgebra
  val t = List(42, 45, 28, 32, 12, 48).ana[Fix[FileSystem]](coAlgebra)
  println(t)

  //тест algebra
  fileSystem.cata(algebra)
}
